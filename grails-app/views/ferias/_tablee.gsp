<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <td><a href="#">Funcionário</a></td>
            <td><a href="#"> Exercício</a></td>
            <td><a href="#">Número de dias</a></td>
            <td><a href="#">Férias Judiciais</a></td>
            <td><a href="#">Observações</a></td>
            <td><a href="#">Justificativa</a></td>
            <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>

        <g:each in="${feriasList}" var="bean" status="i">

            <td>${bean.funcionario}</td>
            <td>${bean.exercicio}</td>
            <td>${bean.numeroDias}</td>
            <td>${bean.feriasJudiciais}</td>
            <td>${bean.observacoes}</td>
            <td>${bean.justificativa}</td>
            <td>
                <g:form resource="${bean}" method="DELETE">
                    <g:link class="btn btn-primary" action="edit" resource="${bean}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="btn btn-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('Deseja remover mesmo?');" />
                </g:form>
            </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>