<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cadastro Grau de Instrução</title>
</head>

<body>
    <div id="graudeinstrucao" class="col-lg-12">
        <fieldset>
            <legend class="wbs-legend-form">Grau de Instrução</legend>

            <div class="row">
                <form class="form">
                    <div class="col-md-8">
                        <div class="form-group  required">
                            <label for="nome" class="col-sm-3 control-label">
                                Nome
                                <span class="required-indicator">*</span>
                            </label>

                            <div class="col-sm-7 col-md-8 col-lg-8">
                                <input type="text" v-model="graudeinstrucao.nome" class="form-control input-sm" name="nome" required="" value="" id="nome">
                            </div>
                        </div>
                        <div class="form-group required">
                            <input type="button" class="btn btn-info" value="Cadastrar" @click="cadastrar"/>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>
        <fieldset>
            <legend class="wbs-legend-form">Listagem</legend>
            <div class="container">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody id="myTable">
                                <tr v-for="f in graudeinstrucoes">
                                    <td>{{f.nome}}</td>

                                    <td>
                                        <a class="btn btn-primary" @click="editar(f)"><i class="fa fa-edit"></i> </a>
                                        <a class="btn btn-danger" @click="remover(f)"><i class="fa fa-remove"></i> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>

</body>
<asset:javascript src="graudeinstrucao-controller.js" asset-defer=""/>
<asset:stylesheet src="style.css"/>
</html>