//component que define o relogio de ponto
Vue.component('ponto-relogio', {
  props: ['value'],
  template: '<div class="ponto-relogio">' +
  ' {{novaHora}}  </div><br/>' +
  '<button type="button" data-toggle="modal" data-target="#myModal"  class="ponto-relogio btn btn-default btn-lg glyphicon glyphicon-time"></button><br/>' +
  '<label>{{value}}</label>' +
  '' +
  ' <!-- Modal -->  \
  <div class="modal fade" id="myModal" role="dialog"> \
  <div class="modal-dialog">  \
  <!-- Modal content-->    \
  <div class="modal-content"> \
  <div class="modal-header"> \
  <button type="button" class="close" data-dismiss="modal">&times;</button>  \
<h4 class="modal-title">Informar Senha</h4> \
</div> \
<div class="modal-body"> \
  <p>Entre com sua senha.</p> \
</div> \
<div class="modal-footer"> \
  <buttontype="button" class="btn btn-default" data-dismiss="modal">Close</button> \
  </div> </div> </div> </div></div>',

  ready(){
    window.setInterval(() => {

      relogio = new Date();

      hora = relogio.getHours();
      minuto = relogio.getMinutes();
      segundo = relogio.getSeconds();

      if(segundo <= 9)  segundo = '0' + segundo;
      if(minuto <= 9)  minuto = '0' + minuto;
      if(hora <= 9)  hora  = '0' + hora ;


      this.horaAgora = hora + ':' + minuto + ':' + segundo;


    }, 1000);

  },

  data: function () {
    return {
      horaAgora: '00:00:00',
      novaHora: '00:00:00'
    }
  },
  watch: {
    horaAgora: function (hora) {
      this.novaHora = hora;
    }

  },
  methods: {
    sincronizar: function () {

    },
    regPonto:function(){
      alert('ponto registrado!');
    }
  }

})

new Vue({
  el: '#app-relogio'


})
