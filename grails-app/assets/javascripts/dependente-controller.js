/**
 * Created by romulofc on 05/04/17.
 */
var app = new Vue({
    el: "#dependente",
    data: {
        dependente: {
            funcionario: {}
        },
        dependentes: []
    },
    created: function(){
        this.listar();
    },
    computed: {
        formatDate: function (date,fmt) {
            return moment(date).format(fmt);
        }
    },
    methods: {
        cadastrar: function() {
            var vue = this;
            if (vue.dependente.id) {
                vue.$http.put('/dependente/update/'+vue.dependente.id+'.json',vue.dependente).then(function(response){
                    vue.dependente = {funcionario:{}};
                    vue.listar();
                });
            } else {
                vue.$http.post('/dependente/save.json',vue.dependente).then(function(response){
                    vue.dependente = {funcionario:{}};
                    vue.listar();
                });
            }
        }
        ,
        listar: function(){
            this.$http.get("/dependente/index.json").then(function(response){
                this.dependentes = response.body;
            });
        },
        editar: function (dependente) {
            this.$http.get("/dependente/show/"+dependente.id+".json").then(function(response){
                this.dependente = response.body;
                this.dependente.dataNascimento = moment(this.dependente.dataNascimento).format('DD/MM/YYYY');
            })
        },
        remover: function(dependente){
            var vue = this;
            bootbox.confirm("Deseja remover <b>"+dependente.nome+"</b>?", function(result){
                if(result){
                    vue.$http.delete('/dependente/delete/'+dependente.id).then(function(){
                        this.listar();
                    });
                }
            });
        }
    }
});







/**
 * Created by Arthur on 31/05/17.
 */
var app = new Vue({
    el: "#funcao",
    data: {
        funcao: {},
        funcoes: []
    },
    created: function(){
        this.listar();
    },
    computed: {
        formatDate: function (date,fmt) {
            return moment(date).format(fmt);
        }
    },
    methods: {
        cadastrar: function() {
            var vue = this;
            if (vue.funcao.id) {
                vue.$http.put('/funcao/update/'+vue.funcao.id+'.json',vue.funcao).then(function(response){
                    vue.listar();
                });
            } else {
                vue.$http.post('/funcao/save.json',vue.funcao).then(function(response){
                    vue.listar();
                });
            }
        }
        ,
        listar: function(){
            this.$http.get("/funcao/index.json").then(function(response){
                this.funcoes = response.body;
            });
        },
        editar: function (funcao) {
            this.$http.get("/funcao/show/"+funcao.id+".json").then(function(response){
                this.funcao = response.body;

            })
        },
        remover: function(funcao){
            var vue = this;
            bootbox.confirm("Deseja remover <b>"+funcao.nome+"</b>?", function(result){
                if(result){
                    vue.$http.delete('/funcao/delete/'+funcao.id).then(function(){
                        this.listar();
                    });
                }
            });
        }
            }
});