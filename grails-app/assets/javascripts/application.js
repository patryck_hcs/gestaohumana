// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery/jquery.js
//= require bootstrap/js/bootstrap.js
//= require vue/vue.js
//= require vue-form/vue-form.js
//= require vue-resource/vue-resource.js
//= require vue-validator/vue-validator.js
//= require select2/select2.full.js
//= require select2/i18n/pt-BR.js
//= require moment/moment.js
//= require bootbox/bootbox.js
//= require jquery.maskedinput/jquery.maskedinput.js
//= require_self


var load = {
    loadMask: function(){
        $(".date-mask").mask("99/99/9999",{placeholder:"dd/mm/aaaa"});
        $(".cpf-mask").mask("999.999.999-99");
        $(".telefone-mask").mask("(99) 99999-9999");
    }
};

jQuery('.numeric').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g,'');
});

jQuery('.char').keyup(function () {
    this.value = this.value.replace(/[^a-zA-Z #]/g,'');
});

$(document).ready(function () {
    $('#bto').click(function () {

        if($('#tes').val() == 1){
            $('#parc').css({"display":"block"});
            $('#abe').css({"display":"block"});
            $('#abe2').css({"display":"none"});
            $('#abe3').css({"display":"none"});
        }else if($('#tes').val() == 2){
            $('#parc').css({"display":"block"});
            $('#abe').css({"display":"block"});
            $('#abe2').css({"display":"block"});
            $('#abe3').css({"display":"none"});
        }else if($('#tes').val() == 3){
            $('#parc').css({"display":"block"});
            $('#abe').css({"display":"block"});
            $('#abe2').css({"display":"block"});
            $('#abe3').css({"display":"block"});
        }else{
            window.alert("Valor inválido!")
        }
    })
    $('.date').mask("99/99/9999"), {placeholder:"dd/mm/aaaa"};
})


