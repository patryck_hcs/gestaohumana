/**
 * Created by Arthur on 01/06/17.
 */
var app = new Vue({
    el: "#grauDeInstrucao",
    data: {
        graudeinstrucao: {},
        graudeinstrucoes: []
    },
    created: function(){
        this.listar();
    },
    computed: {
        formatDate: function (date,fmt) {
            return moment(date).format(fmt);
        }
    },
    methods: {
        cadastrar: function() {
            var vue = this;
            if (vue.graudeinstrucao.id) {
                vue.$http.put('/grauDeInstrucao/update/'+vue.graudeinstrucao.id+'.json',vue.graudeinstrucao).then(function(response){
                    vue.listar();
                });
            } else {
                vue.$http.post('/grauDeInstrucao/save.json',vue.graudeinstrucao).then(function(response){
                    vue.listar();
                });
            }
        }
        ,
        listar: function(){
            this.$http.get("/grauDeInstrucao/index.json").then(function(response){
                this.graudeinstrucoes = response.body;
            });
        },
        editar: function (graudeinstrucao) {
            this.$http.get("/graudeinstrucao/show/"+graudeinstrucao.id+".json").then(function(response){
                this.graudeinstrucao = response.body;

            })
        },
        remover: function(V){
            var vue = this;
            bootbox.confirm("Deseja remover <b>"+V.nome+"</b>?", function(result){
                if(result){
                    vue.$http.delete('/grauDeInstrucao/delete/'+V.id).then(function(){
                        this.listar();
                    });
                }
            });
        }
            }
});