package rh.mod.ponto

import br.edu.unirn.Funcionario

class RegistroPonto{

  Date entrada
  Date saida
  static belongsTo = [funcionario:Funcionario]


  static  constraints = {

    funcionario()
    entrada()
    saida()



  }


}
