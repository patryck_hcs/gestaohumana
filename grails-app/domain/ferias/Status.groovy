package ferias
/*Classe responsável pelo status de férias do funcionário
PATRYCK HERNANDEZ*/

class Status {

    String descricao
    boolean ativo
    static belongsTo = Ferias
    static constraints = {
        descricao()
        ativo()

    }
}
