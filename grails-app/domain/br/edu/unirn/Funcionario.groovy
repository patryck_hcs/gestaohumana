package br.edu.unirn

import ferias.Ferias
import grails.databinding.BindingFormat
import rh.mod.ponto.Ponto

class Funcionario {

    String nome

    @BindingFormat("dd/MM/yyyy")
    Date dataNascimento
    GrauDeInstrucao graudeinstrucao
    String cpf
    Funcao funcao
    String email
    String telefonePrincipal
    long rg
    String orgaoEmissorRg
    String cidade
    String estado
    String estadoCivil
    String sexo
    String conjugue
    String nomePai
    String nomeMae
    String endereco

    @BindingFormat("dd/MM/yyyy")
    Date dataAdmissao

    String tituloEleitor
    String carteiraTrabalho
    String serieCarteiraTrabalho
    String banco
    long agenciaBancaria
    long contaBancaria
    long numeroReservista

    @BindingFormat("dd/MM/yyyy")
    Date dataEmissaoRg
    long pisPasep




    Date dateCreated
    Date lastUpdated

    static constraints = {
        nome()
        cpf unique:true
        funcao()
        email email:true
        dataNascimento()
        graudeinstrucao()
        telefonePrincipal()
        dataAdmissao()
        estado()
        cidade()
        endereco()
        estadoCivil() //inList:["Solteiro(a)", "Casado(a)", "Divorciado(a)", "Viúvo(a)", "Separado(a)"]
        conjugue nullable:true
        nomePai()
        nomeMae()
        sexo() //inList:["Masculino", "Feminino"]
        rg()
        orgaoEmissorRg()
        tituloEleitor()
        carteiraTrabalho()
        serieCarteiraTrabalho()
        banco()
        agenciaBancaria()
        contaBancaria()
        dataEmissaoRg()
        pisPasep()
    }
    static hasMany = [ferias:Ferias]

    String toString(){
        nome
    }
}
