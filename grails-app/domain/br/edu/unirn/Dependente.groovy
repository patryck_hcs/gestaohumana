package br.edu.unirn

import grails.databinding.BindingFormat

class Dependente {

    Funcionario funcionario
    String tipo
    String nome

    @BindingFormat("dd/MM/yyyy")
    Date dataNascimento

    static constraints = {

        funcionario()
        tipo()
        nome()
        dataNascimento()
    }
}
