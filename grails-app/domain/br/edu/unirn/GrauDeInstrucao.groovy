package br.edu.unirn

class GrauDeInstrucao {

    String nome

    static constraints = {
        nome()
    }

    String toString(){
        nome
    }
}
