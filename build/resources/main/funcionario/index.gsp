<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cadastro de Funcionário</title>
</head>

<body>
<div id="funcionario" class="col-lg-12">
    <fieldset>
        <legend class="wbs-legend-form">Funcionário</legend>

        <div class="row">
            <form class="form">
                <div class="col-md-8">
                    <div class="form-group  required">
                        <label for="nome" class="col-sm-3 control-label">
                            Nome
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-7 col-md-8 col-lg-8">
                            <input type="text" v-model="funcionario.nome" class="form-control input-sm" name="nome" required="" value="" id="nome">
                        </div>
                    </div>

                    <div class="form-group  required">
                        <label for="dataNascimento" class="col-sm-3 control-label">
                            Dt. Nascimento
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" v-model="funcionario.dataNascimento" class="form-control input-sm date-mask" name="dataNascimento" required="" id="dataNascimento">
                        </div>
                        <label for="graudeinstrucao" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Grau Instrução
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <g:select name="graudeinstrucao" v-model="funcionario.graudeinstrucao.id" optionKey="id" from="${br.edu.unirn.GrauDeInstrucao.list()}" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group  required">
                        <label for="cpf" class="col-sm-3 control-label">
                            CPF
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="cpf" v-model="funcionario.cpf" class="form-control input-sm cpf-mask"
                                   id="cpf">

                        </div>
                        <label for="funcao" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Função
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <g:select name="funcao" v-model="funcionario.funcao.id" optionKey="id" from="${br.edu.unirn.Funcao.list()}" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="email" class="col-sm-3 control-label">
                            E-mail
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="email" name="email" v-model="funcionario.email" class="form-control input-sm" required=""
                                   id="email">

                        </div>
                        <label for="telefonePrincipal" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Telefone Princ.
                            <span class="required-indicator">*</span>
                        </label>

                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="telefonePrincipal" v-model="funcionario.telefonePrincipal" class="form-control input-sm telefone-mask" required=""
                                   id="telefonePrincipal">
                        </div>
                    </div>

                    <%-- RG e Orgão Emissor do RG --%>
                    <div class="form-group required">
                    <label for="rg" class="col-sm-3 control-label">
                        RG
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="rg" v-model="funcionario.rg" class="form-control input-sm" id="rg">
                        </div>
                    <label for="orgaoEmissorRg" class="col-sm-2 col-md-2 col-lg-2 control-label">
                        Orgão Emissor
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="orgaoEmissorRg" v-model="funcionario.orgaoEmissorRg" class="form-control input-sm" id="orgaoEmissorRg">
                        </div>
                    </div>

                    <%-- Campo Data de Emissão do RG --%>
                    <div class="form-group  required">
                        <label for="dataEmissaoRg" class="col-sm-3 control-label">
                            Dt. Emissão do RG
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-7 col-md-8 col-lg-8">
                        <input type="text" v-model="funcionario.dataEmissaoRg" class="form-control input-sm date-mask" name="dataEmissaoRg" required="" id="dataEmissaoRg">
                    </div>
                    </div>

                    <%-- Cidade Natal e Estado --%>
                    <div class="form-group required">
                        <label for="cidade" class="col-sm-3 control-label">
                        Cidade de Nascimento
                        <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" name="cidade" v-model="funcionario.cidade" class="form-control input-sm" id="cidade">
                    </div>
                    <label for="estado" class="col-sm-2 col-md-2 col-lg-2 control-label">
                        Estado de Nas.
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                    <select name="estado" v-model="funcionario.estado" id="estado" class="form-control">
                            <option>Acre</option>
                            <option>Alagoas</option>
                            <option>Amapá</option>
                            <option>Amazonas</option>
                            <option>Bahia</option>
                            <option>Ceará</option>
                            <option>Distrito Federal</option>
                            <option>Espírito Santo</option>
                            <option>Goiás</option>
                            <option>Maranhão</option>
                            <option>Mato Grosso</option>
                            <option>Mato Grosso do Sul</option>
                            <option>Minas Gerais</option>
                            <option>Pará</option>
                            <option>Paraíba</option>
                            <option>Paraná</option>
                            <option>Pernambuco</option>
                            <option>Piauí</option>
                            <option>Rio de Janeiro</option>
                            <option>Rio Grande do Norte</option>
                            <option>Rio Grande do Sul</option>
                            <option>Rondônia</option>
                            <option>Roraima</option>
                            <option>Santa Catarina</option>
                            <option>São Paulo</option>
                            <option>Sergipe</option>
                            <option>Tocantins</option>
                    </select>
                    </div>
                    </div>

                    <%-- Estado Civil e Sexo --%>
                    <div class="form-group required">
                        <label for="estadoCivil" class="col-sm-3 control-label">
                            Estado Civil
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                    <select name="estadoCivil" v-model="funcionario.estadoCivil" id="estadoCivil" class="form-control">
                                <option>Solteiro(a)</option>
                                <option>Casado(a)</option>
                                <option>Divorciado(a)</option>
                                <option>Viúvo(a)</option>
                                <option>Separado(a)</option>
                    </select>
                    </div>
                        <label for="sexo" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Sexo
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <select name="sexo" v-model="funcionario.sexo" id="sexo" class="form-control">
                                <option>Masculino</option>
                                <option>Feminino</option>
                        </select>
                    </div>
                    </div>

                    <%-- Campo Cônjugue --%>
                    <div class="form-group">
                        <label for="conjugue" class="col-sm-3 control-label">
                             Nome do Cônjugue
                        </label>
                    <div class="col-sm-7 col-md-8 col-lg-8">
                        <input type="text" v-model="funcionario.conjugue" class="form-control input-sm" name="conjugue" value="" id="conjugue">
                    </div>
                    </div> 
                   
                    <%-- Campo Nome do Pai e Mãe --%>
                    <div class="form-group  required">
                        <label for="nomePai" class="col-sm-3 control-label">
                         Nome do Pai
                            <span class="required-indicator">*</span>
                        </label>
                           <div class="col-sm-7 col-md-8 col-lg-8">
                              <input type="text" v-model="funcionario.nomePai" class="form-control input-sm" name="nomePai" required="" value="" id="nomePai">
                           </div>
                    </div>
                    <%-- Campo Cônjugue --%>
                    <div class="form-group  required">
                        <label for="nomeMae" class="col-sm-3 control-label">
                    Nome da Mãe
                        <span class="required-indicator">*</span>
                        </label>
                            <div class="col-sm-7 col-md-8 col-lg-8">
                                <input type="text" v-model="funcionario.nomeMae" class="form-control input-sm" name="nomeMae" required="" value="" id="nomeMae">
                            </div>
                    </div>
                    
                    <%-- Campo Endereço --%>
                    <div class="form-group  required">
                        <label for="endereco" class="col-sm-3 control-label">
                         Endereço
                            <span class="required-indicator">*</span>
                        </label>
                           <div class="col-sm-7 col-md-8 col-lg-8">
                              <input type="text" v-model="funcionario.endereco" class="form-control input-sm" name="endereco" required="" value="" id="endereco">
                           </div>
                    </div>

                    <%-- Campo Data de Admissão e Título Eleitor --%>
                    <div class="form-group required">
                        <label for="dataAdmissao" class="col-sm-3 control-label">
                            Dt. de Admissão
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" name="dataAdmissao" v-model="funcionario.dataAdmissao" class="form-control input-sm date-mask" id="dataAdmissao">
                    </div>
                        <label for="tituloEleitor" class="col-sm-2 col-md-2 col-lg-2 control-label">
                            Título Eleitor
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" name="tituloEleitor" v-model="funcionario.tituloEleitor" class="form-control input-sm" id="tituloEleitor">
                    </div>
                    </div>

                    <%-- Campo Número e Serie da Carteira de Trabalho --%>
                    <div class="form-group required">
                        <label for="carteiraTrabalho" class="col-sm-3 control-label">
                            Num. Carteira Trabalho
                            <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" name="carteiraTrabalho" v-model="funcionario.carteiraTrabalho" class="form-control input-sm" id="carteiraTrabalho">
                    </div>
                    <label for="serieCarteiraTrabalho" class="col-sm-2 col-md-2 col-lg-2 control-label">
                        Série Carteira
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="serieCarteiraTrabalho" v-model="funcionario.serieCarteiraTrabalho" class="form-control input-sm" id="serieCarteiraTrabalho">
                        </div>
                    </div>
                    
                    <%-- Campo Banco e Agência --%>
                    <div class="form-group required">
                    <label for="banco" class="col-sm-3 control-label">
                        Banco
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="banco" v-model="funcionario.banco" class="form-control input-sm" id="banco">
                        </div>
                    <label for="agenciaBancaria" class="col-sm-2 col-md-2 col-lg-2 control-label">
                        Agência
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="agenciaBancaria" v-model="funcionario.agenciaBancaria" class="form-control input-sm" id="agenciaBancaria">
                        </div>
                    </div>
                    <%-- Campo Conta Bancária --%>
                    <div class="form-group required">
                        <label for="contaBancaria" class="col-sm-3 control-label">
                            Conta Bancária
                    <span class="required-indicator">*</span>
                        </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" name="contaBancaria" v-model="funcionario.contaBancaria" class="form-control input-sm" id="contaBancaria">
                    </div>
                    <label for="numeroReservista" class="col-sm-2 col-md-2 col-lg-2 control-label">
                        Núm. Reservista
                        <span class="required-indicator">*</span>
                    </label>
                        <div class="col-sm-4 col-md-3 col-lg-3">
                            <input type="text" name="numeroReservista" v-model="funcionario.numeroReservista" class="form-control input-sm" id="numeroReservista">
                        </div>
                    </div>

                    <%-- Campo Grau de Instrucao --%>
                    <div class="form-group  required">
                    <label for="pisPasep" class="col-sm-3 control-label">
                       Número do PIS/PASEP
                        <span class="required-indicator">*</span>
                    </label>
                    <div class="col-sm-4 col-md-3 col-lg-3">
                        <input type="text" v-model="funcionario.pisPasep" class="form-control input-sm" name="pisPasep" required="" value="" id="pisPasep" >
                    </div>
                    </div>


                    <div class="form-group required">
                        <input type="button" class="btn btn-info" value="Cadastrar" @click="cadastrar"/>
                    </div>
                </div>
            </form>
        </div>
    </fieldset>
    <fieldset>
        <legend class="wbs-legend-form">Listagem</legend>
        <div class="container">
        <div class="row">
        <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Data de Nascimento</th>
                    <th>CPF</th>
                    <th>Telefone Principal</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody id="myTable">
                <tr v-for="f in funcionarios">
                    <td>{{f.nome}}</td>
                    <td>{{f.dataNascimento | formatDate:'dd\/MM\/yyyy'}}</td>
                    <td>{{f.cpf}}</td>
                    <td>{{f.telefonePrincipal}}</td>
                    <td>
                        <a class="btn btn-primary" @click="editar(f)"><i class="fa fa-edit"></i> </a>
                        <a class="btn btn-danger" @click="remover(f)"><i class="fa fa-remove"></i> </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>

</body>
<asset:javascript src="filho-controller.js" asset-defer=""/>
<asset:javascript src="funcionario-controller.js" asset-defer=""/>
<asset:stylesheet src="style.css"/>
</html>