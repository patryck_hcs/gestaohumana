<div class="princ">
<div class="form-group tesv ${invalid ? 'has-error has-feedback' : required && value ? 'has-success has-feedback' : ''}">
    <label class="col-xs-6 col-sm-4 lab" for="${property}">Número de Parcelas</label>
    <div class="tes">
        <f:input bean="${bean}" id="tes" name="numeroParcelas" property="${property}" class="inpt4"/>
        <g:if test="${invalid}">
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">${errors.join('<br>')}</span>
        </g:if>

    </div>
</div>
</div>
