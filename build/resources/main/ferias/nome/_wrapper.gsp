<div class="princ">
<div class="form-group ${invalid ? 'has-error has-feedback' : required && value ? 'has-success has-feedback' : ''}">
    <label class="col-xs-6 col-sm-4 lab" for="${property}">Funcionário</label>
    <div class="col-sm-9">
        <g:textField name="nome"  class="inpt" placeholder="NOME"/>
        <g:if test="${invalid}">
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">${errors.join('<br>')}</span>
        </g:if>
        <g:else test="${invalid}">
            <g:if test="${required && value}">
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </g:if>
        </g:else>
    </div>
</div>
</div>
