<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'ferias.label', default: 'Ferias')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
    <button class="sidebar-toggle visible-xs" id="toggle-left-mini-aside">
        <span class="glyphicon glyphicon-align-justify"></span>
    </button>
    <g:if test="${flash.message}">
        <div class="suces">
            <i class="fa fa-check-circle" aria-hidden="true"></i>
            ${flash.message}
        </div>
    </g:if>
    <div class="row">
        <div class="col-sm-12">
            <h4 style="font-weight: 300;display: inline-block;"><g:message code="default.list.label"
                                                                           args="[entityName]"/></h4>
            <g:link action="create" class="btn btn-primary btn-flat btn-sm pull-right">
                <i class="glyphicon glyphicon-plus"></i> <g:message code="default.create.label"
                                                                    args="[entityName]"/>
            </g:link>
        </div>

        <div class="col-sm-12">
           <g:render template="tablee" bean="${feriasList}"/>

            <div class="pagination">
                <g:paginate  total="${feriasCount ?: 4}"/>
            </div>
        </div>
    </div>
</body>
</html>
