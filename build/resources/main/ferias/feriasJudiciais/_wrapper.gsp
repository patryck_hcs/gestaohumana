<div class="princRad">
<div class="form-group ${invalid ? 'has-error has-feedback' : required && value ? 'has-success has-feedback' : ''}">
    <label class="col-sm-3 control-label" for="${property}">Férias judiciais</label>
    <div class="col-sm-9">
<g:radioGroup name="${property}" labels="['Sim','Não']" values="['sim','não']" value="${value}">
    <span class="rad">${it.radio} ${it.label}</span>
</g:radioGroup>
        <g:if test="${invalid}">
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">${errors.join('<br>')}</span>
        </g:if>

    </div>
</div>
</div>