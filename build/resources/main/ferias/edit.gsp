<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'ferias.label', default: 'Ferias')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="row">
    <div class="col-sm-12">
        <h4 style="font-weight: 300;display: inline-block;margin-bottom: 25px;"><g:message code="default.edit.label"
                                                                                           args="[entityName]"/></h4>
        <g:link action="create" class="btn btn-primary btn-flat btn-sm pull-right margin-left-15">
            <i class="glyphicon glyphicon-plus"></i> <g:message code="default.create.label" args="[entityName]"/>
        </g:link>
        <g:link class="btn btn-default btn-sm btn-flat pull-right" action="index"><i
                class="glyphicon glyphicon-arrow-left"></i> <g:message code="default.back.label"
                                                                       default="voltar"/></g:link>
    </div>

    <div class="col-sm-12">
        <g:if test="${flash.message}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                ${flash.message}
            </div>
        </g:if>
        <g:hasErrors bean="${ferias}">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <i class="glyphicon glyphicon-exclamation-sign"></i> Alguns campos precisam ser corrigidos.
            </div>
        </g:hasErrors>
        <div class="feria">
            <header class="head"> <h4>
                <i class="fa fa-car" aria-hidden="true"></i>  Férias
            </h4>
            </header>
            <div class="linha"></div>
            <div class="feria2">
        <g:form class="form-horizontal" name="feriasForm" novalidate="novalidate"
                resource="${this.ferias}" method="PUT">
            <g:hiddenField name="version" value="${ferias?.version}"/>
            <fieldset class="form">
                <div class="divnom">
                <div class="labfunc">
                <label>Funcionário</label>
                </div>
                <g:textField name="func" class="inpt5" value="${ferias.funcionario}"/>
                </div>
                <f:field bean="ferias" property="exercicio"/>
                <f:field bean="ferias" property="numeroDias"/>
                <f:field bean="ferias" property="observacoes"/>
                <f:field bean="ferias" property="justificativa"/>
                <f:field bean="ferias" property="feriasJudiciais"/>
                <f:field bean="ferias" property="numeroParcelas"/>
                <div class="teste">
                <a href="javascript:void(0);" id="bto">Exibir Períodos</a>
                </div>
                <div id="parc" class="mostPer">
                    <header class="tit"><h4>
                        <i class="fa fa-car" aria-hidden="true"></i>  Dados do Parcelamento
                    </h4>
                    </header>

                    <div id="abe" class="diaT">
                        <div class="loed">
                            <p>Períodos</p>
                            <p>Dias</p>
                            <p>Início</p>
                            <p>Término</p>
                        </div>
                        <label>1º Período</label>
                        <g:textField class="erf" name="dia1"/>
                        <g:textField class="dta" precision="day" name="data1"/>
                    </div>

                    <div id="abe2" class="diaT">
                        <label>2º Período</label>
                        <g:textField class="erf" name="dia2"/>
                        <g:textField class="dta" precision="day" name="data2"/>
                    </div>

                    <div id="abe3" class="diaT">
                        <label>3º Período</label>
                        <g:textField class="erf" name="dia3"/>
                        <g:textField class="dta" precision="day" name="data3"/>
                    </div>

                </div>
            </fieldset>
        </g:form>
            </div>
        </div>
        <div class="panel-footer clearfix form-actions"
             style="margin-top: 25px;background-color: #fff; border-radius: 0;">
            <button type="submit" form="feriasForm" class="btn btn-success btn-sm btn-flat btt">
                <i class="fa fa-edit"></i> <g:message code="default.button.update.label"
                                                                  args="[entityName]"/>
            </button>
        </div>
    </div>
</div>
</body>
</html>
