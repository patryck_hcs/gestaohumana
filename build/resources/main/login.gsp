<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Página Inicial</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<section class="login-info">
    <div class="container">
        <div class="row mained">
            <div class="form-header">
                <h1 class="text-center ">Login </h1>
            </div>
            <div class="main-content">

                <g:form controller="auth" name="formAuth" action="log">
                    <g:render template="/layouts/logs"/>
                        <g:actionSubmit class="btn btn-danger btn-lg btn-block login-button" value="login"/>
                    </div>
                </g:form>
              %{--  <div class="panel-footer clearfix form-actions"
                     style="margin-top: 25px;background-color: #fff; border-radius: 0;">
                    <button type="submit" form="formAuth" class="btn btn-danger btn-lg btn-block login-button">
                        <i class="glyphicon glyphicon-ok"></i> <g:message code="login"/>
                    </button>
                </div>--}%

                %{--<div class="input-group ">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></span>
                    <input id="email" type="text" class="form-control" name="email" placeholder="Enter your Email">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
                    <input id="password" type="password" class="form-control" name="password" placeholder="Enter your Password">
                </div>

                <div class="checkbox">
                    <label class="remember">
                        <input name="remember" type="checkbox" > Remember Me
                    </label>
                </div>

                <div class="form-group ">
                    <a href="#" type="button"  class="btn btn-danger btn-lg btn-block login-button">login</a>
                </div>--}%



            </div>
        </div>
    </div>
</section>
</body>
</html>
