package ferias

import grails.databinding.BindingFormat

class Parcelas {
    @BindingFormat('yyyy-MM-dd')
    Date dataSaida
    @BindingFormat('yyyy-MM-dd')
    Date dataVolta
    String status
    Ferias ferias
    static constraints = {
        dataSaida()
        dataVolta()
        status()
    }

}
