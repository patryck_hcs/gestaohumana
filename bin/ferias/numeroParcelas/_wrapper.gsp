<div class="princ">
<div class="form-group ${invalid ? 'has-error has-feedback' : required && value ? 'has-success has-feedback' : ''}">
    <label class="col-xs-6 col-sm-4 lab" for="${property}">Número de Parcelas</label>
    <div class="col-sm-9">
        <f:input bean="${bean}" property="${property}" class="inpt4"/>
        <g:if test="${invalid}">
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">${errors.join('<br>')}</span>
        </g:if>
        <g:else test="${invalid}">
            <g:if test="${required && value}">
                <span class="glyphicon glyphicon-ok form-control-feedback"></span>
            </g:if>
        </g:else>
    </div>
</div>
</div>
