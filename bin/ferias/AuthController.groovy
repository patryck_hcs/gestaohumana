package ferias
/*Patryck Hernandez*/
import gestaohumana.User
import grails.transaction.Transactional

class AuthController {

    def login() {
        render(view: '/login')
    }

    @Transactional
    def log(){
        def u = User.findByUsername(params.username)
        if (u) {
            if (u.password == params.password) {
                session.user = u
                redirect(action: "home")
            }
            else {
                render(view: "login", model: [message: "Password incorrect"])
            }
        }
        else {
            render(view: "login", model: [message: "User not found"])
        }

    }
}
