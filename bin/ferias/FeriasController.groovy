package ferias

import br.edu.unirn.Funcionario

import java.sql.Date
import java.text.DateFormat
import java.text.SimpleDateFormat

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FeriasController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Ferias.list(), model:[feriasCount: Ferias.count()]
    }

    def create() {
        if (params.nome != null) {
            def nome = Funcionario.findByNomeLike("%" + params.nome + "%")
            def fer = Ferias.findByFuncionario(nome)

            if (fer == null) {

                respond new Ferias(params)

            } else {

                respond new Ferias(params), view: 'create', model: [fer: fer, nome: nome]
            }
        }
    }

    @Transactional
    def save(Ferias ferias) {
        if (ferias == null) {
            notFound()
            return
        }
            Funcionario res = Funcionario.findByNomeLike("%"+params.nom.nome+"%")

                if(res == null){
                    flash.message = "Funcionário não existe!"
                }else{
                   ferias.funcionario = res
                }
            ferias.save(flush:true, failOnError: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action:"index", method:"GET"
            }
            '*' { respond ferias, [status: CREATED] }
        }
    }

    def edit(Ferias ferias) {
        respond ferias
    }

    @Transactional
    def update(Ferias ferias) {
        if (ferias == null) {
            notFound()
            return
        }

        if (ferias.hasErrors()) {
            respond ferias.errors, view:'edit'
            return
        }

        ferias.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action:"index", method:"GET"
            }
            '*'{ respond ferias, [status: OK] }
        }
    }

    @Transactional
    def delete(Ferias ferias) {

        if (ferias == null) {
            notFound()
            return
        }
        if(new Ferias()?.properties?.containsKey('ativo')){
            ferias.ativo = false
            ferias.save flush:true
        }else{
            ferias.delete flush:true
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ferias.label', default: 'Ferias'), ferias.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ferias.label', default: 'Ferias'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
