package br.edu.unirn

class Funcao {

    String nome
    String observacao

    static constraints = {
      nome()
      observacao widget:"textarea",nullable:true
    }

    String toString(){
      nome
    }
}
