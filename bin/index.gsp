<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Página Inicial</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
</head>

<body>
<div id="controllers" class="modulos">
    <h2>Módulos:</h2>
    <ul>
        <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName }}">
            <li class="controller">
                <g:link controller="${c.logicalPropertyName}">${c.name}</g:link>
            </li>
        </g:each>
    </ul>
</div>


</body>
</html>
